# Weekly Coding Challenges
Solutions to Revature on-boarding's weekly coding challenges

## Description
All solutions found in https://gitlab.com/christian_seefeldt/weekly-coding-challenge/-/tree/main/com.challenges/src/main/java/com/challenges

## Authors and acknowledgment
Christian D. Seefeldt
