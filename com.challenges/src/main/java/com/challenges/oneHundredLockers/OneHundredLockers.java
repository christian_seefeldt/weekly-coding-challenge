package com.challenges.oneHundredLockers;

import java.util.Scanner;

public class OneHundredLockers {
	
	public static void main (String[]args) {
		Scanner scan = new Scanner(System.in);
		
		int numberOfLockers = scan.nextInt();
				
		int[] cycledLockers = cycleLockers(numberOfLockers);
		
		System.out.println("Number of Open Lockers: " + countOpenLockers(cycledLockers));

		scan.close();
}
	
	public static int[] cycleLockers(int numberOfLockers) {
		int[] lockers = new int[numberOfLockers];
		for(int i = 1; i < numberOfLockers + 1; i++) {
			for(int j = i - 1; j < numberOfLockers; j += i) {
					if(lockers[j] == 0) {
						lockers[j] = 1;
					} else lockers[j] = 0;
			}
		}
		return lockers;
	}
	
	public static int countOpenLockers(int[] lockers) {
			int numberOfOpenLockers = 0;
			for(int i = 0; i < lockers.length; i++) {
			if(lockers[i] == 1) {
				numberOfOpenLockers++;
			}
		}
		return numberOfOpenLockers;
	}
}


