package com.challenges.oneHundredLockers;

import java.util.Scanner;

public class OneHundredLockersUsingSquraeRoot {
	
	public static void main (String[]args) {
		Scanner scan = new Scanner(System.in);
		
		int numberOfLockers = scan.nextInt();
		
		double openLocker = Math.sqrt(numberOfLockers);
		
		String openLockerCount = Double.toString(openLocker).split("\\.", 2)[0];
		
		System.out.println("Number of Open Lockers: " + openLockerCount);

		scan.close();
	}
}
