package com.challenges.projectBuildOrder;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class BuildOrder {
	
	public static void main (String[]args) {
		
		String[] projects = {"a","b","c","d","e","f"};
		
//		String[][] dependencies = null;
		
		String[][] dependencies = 	{{"a","f","b","f","d"},
									{"d","b","d","a","c"}};
		
		try {
			CalculateBuildOrder(projects, dependencies);
		} catch (Exception e) {
			System.out.println("No Valid Build Order");
		}
		
	}
		
		private static void CalculateBuildOrder(String[] projects,String[][] dependencies) throws Exception {
//			
			String order = CalcIndependent(projects, dependencies);
			
			order = CalcDependent(projects, dependencies,order);
			
			System.out.println(order);
		}

		private static String CalcIndependent(String[] projects,String[][] dependencies) {
			String order = "";
			boolean independent = true;
			
			for(int i = 0; i < projects.length; i++) {
				independent = true;
				for(int j = 0; j < dependencies[0].length; j++) {
					if(projects[i] == dependencies[1][j]) independent = false;
				}
				if(independent) order = order + projects[i] + ", ";
			}
			
			return order;
		}
		
		private static String CalcDependent(String[] projects, String[][] dependencies, String order) throws Exception {
			
			for(int i = 0; i < projects.length; i++) {
				String tried = null;
				String dependent = null;
				boolean buildable = true;
				if(!order.contains(projects[i])) {
					for(int j = 0; j < dependencies[0].length; j++) {
						if(projects[i] == dependencies[1][j] && !order.contains(dependencies[0][j])) {
							buildable = false;
							dependent = dependencies[0][j];
							tried = projects[i];
						}
					}
					if(buildable) {
						order = order + projects[i] + ", ";
					} else {
						order = CalcDependentIndex(projects, dependencies,order, dependent, tried);
						order = order + projects[i] + ", ";
					}
				}
			}
			return order;
		}
		
		private static String CalcDependentIndex(String[] projects, String[][] dependencies, String order, String dep, String tried) throws Exception {
			String dependent = null;
			boolean buildable = true;
			
			if(tried.contains(dep)) throw new Exception("No Valid Build Order");
			
			for(int i = 0; i < dependencies[0].length; i++) {
				if(dep == dependencies[1][i] && !order.contains(dependencies[0][i])) {
					buildable = false;
					dependent = projects[i];
					tried = tried + dep;
				}
			}
			if(buildable) {
				order = order + dep + ", ";
			} else {
				CalcDependentIndex(projects, dependencies,order, dependent, tried);
			}
			
			return order;
		}
//		try {
//			CalculateBuildOrder(projects, dependencies);
//		} catch (Exception e) {
//			System.out.println("No Valid Build Order");
//		}
//	}
//
//	private static void CalculateBuildOrder(String[] projects,String[][] dependencies) throws Exception {
//		
//		if(dependencies == null) {
//			for(int i = 0; i < projects.length - 1; i++) {
//				System.out.print(projects[i] + ", ");
//			}
//			System.out.print(projects[projects.length - 1]);
//			return;
//		}
//		String order = "";
//		
//		String[] tmpProject = new String[projects.length];
//		
//		for(int i = 0; i < projects.length; i++) {
//			order = calcBuild(order, projects, dependencies, i, tmpProject);
//		}
//		
//		System.out.println(order);
//	}
//
//	private static String calcBuild(String order, String[] projects, String[][] dependencies, int index, String[] tmpProject) throws Exception {
//		for(int i = 0; i < dependencies[0].length; i++) {
//			if(dependencies[1][i] == projects[index]) {
//				if(Arrays.asList(tmpProject).contains(projects[index])) throw new Exception("No Valid Build Order");
//				tmpProject[index] = projects[index];
//				for(int j = 0; j < projects.length; j ++) {
//					if(projects[j] == dependencies[0][i]) index = j;
//				}
//				order = calcBuild(order, projects, dependencies, index, tmpProject);
//			}
//			if(!order.contains(projects[index])) order = order + projects[index] + ", ";
//			tmpProject = new String[projects.length];
//		}
//		return order;
//	}

}
