package com.challenges.circularArray;

import java.util.Arrays;
import java.util.Iterator;

public class CircularArray<T> implements Iterable<T> {

	public T[] arr;
	
	public CircularArray(T[] arr) {
		this.arr = arr;
	}
	
	public T[] getCirArr() {
		return arr;
	}
	
	public T getArrCirIndex(int index) {
		return arr[index];
	}
	
	public int getCirSize() {
		return arr.length;
	}
	
	public void setCirArr(T[] arr) {
		this.arr = arr;
	}
	
	public void print() {
		System.out.println(Arrays.toString(arr));
	}

	public void rotate(int amount) {
		for(int i = 0; i < amount; i++) {
			rotateCir();
		}
	}
	
	public void rotateReverse(int amount) {
		for(int i = 0; i < amount; i++) {
			rotateCirReverse();
		}
	}
	
	public void rotateCir() {
		T tail = arr[arr.length-1];
		
		for(int i = arr.length - 1; i > 0; i--) {
			arr[i] = arr[i-1];
		}
		arr[0] = tail;
	}

	public void rotateCirReverse() {
		T head = arr[0];
		
		for(int i = 0; i < arr.length - 1; i++) {
			arr[i] = arr[i+1];
		}
		arr[arr.length-1] = head;
	}

	@Override
	public Iterator<T> iterator() {
		return Arrays.stream(arr).iterator();
	}
}
