package com.challenges.circularArray;

import java.util.Arrays;
import java.util.Iterator;

public class CicularArrayTester {

	public static void main(String[] args) {
		
		System.out.println("Integer Circle:");
		IntegerCircle();
		
		System.out.println("\nString Circle:");
		StringCircle();
		
		System.out.println("\nDouble Circle:");
		DoubleCircle();
		
		System.out.println("\nLong Circle:");
		LongCircle();
		
		System.out.println("\nBoolean Circle:");
		BooleanCircle();
		
		System.out.println("\nObject Circle:");
		ObjectCircle();
		
		System.out.println("\nChar Circle:");
		CharCircle();
		
		System.out.println("\nByte Circle:");
		ByteCircle();
		
		System.out.println("\nShort Circle:");
		ShortCircle();
		
		System.out.println("\nFloat Circle:");
		FloatCircle();
	}
	
	public static void IntegerCircle() {
		
		Integer[] intArr = new Integer[] {1, 2, 3, 4, 5};
		
		CircularArray circleArr = new CircularArray(intArr);
		
		System.out.println(Arrays.toString(circleArr.getCirArr()));
		
		circleArr.rotate(1);
		
		circleArr.print();
		
		circleArr.rotateReverse(1);
		
		circleArr.print();
		
		Iterator<Integer> it = circleArr.iterator();
		
		while(it.hasNext()) {
			  System.out.print(it.next() + " ");
			}
		
		System.out.println("\n" + circleArr.getArrCirIndex(2));
	}
	
	public static void StringCircle() {
		
		String[] strArr = new String[] {"1", "2", "3", "4", "5"};
		
		CircularArray circleArr = new CircularArray(strArr);
		
		System.out.println(Arrays.toString(circleArr.getCirArr()));
		
		circleArr.rotate(2);
		
		circleArr.print();
		
		circleArr.rotateReverse(2);
		
		circleArr.print();
		
		Iterator<String> it = circleArr.iterator();
		
		while(it.hasNext()) {
			  System.out.print(it.next() + " ");
			}
		
		System.out.println("\n" + circleArr.getArrCirIndex(2));
	}
	
	public static void DoubleCircle() {
		
		Double[] dblArr = new Double[] {1.11, 2.22, 3.33, 4.44, 5.55};
		
		CircularArray circleArr = new CircularArray(dblArr);
		
		System.out.println(Arrays.toString(circleArr.getCirArr()));
		
		circleArr.rotate(3);
		
		circleArr.print();
		
		circleArr.rotateReverse(3);
		
		circleArr.print();
		
		Iterator<Double> it = circleArr.iterator();
		
		while(it.hasNext()) {
			  System.out.print(it.next() + " ");
			}
		
		System.out.println("\n" + circleArr.getArrCirIndex(2));
	}
	
	public static void LongCircle() {

		Long[] longArr = new Long[] {1L, 2L, 3L, 4L, 5L};
		
		CircularArray circleArr = new CircularArray(longArr);
		
		System.out.println(Arrays.toString(circleArr.getCirArr()));
		
		circleArr.rotate(4);
		
		circleArr.print();
		
		circleArr.rotateReverse(4);
		
		circleArr.print();
		
		Iterator<Long> it = circleArr.iterator();
		
		while(it.hasNext()) {
			  System.out.print(it.next() + " ");
			}
		
		System.out.println("\n" + circleArr.getArrCirIndex(2));
	}
	
	public static void BooleanCircle() {

		Boolean[] boolArr = new Boolean[] {true, false, null, false, true};
		
		CircularArray circleArr = new CircularArray(boolArr);
		
		System.out.println(Arrays.toString(circleArr.getCirArr()));
		
		circleArr.rotate(4);
		
		circleArr.print();
		
		circleArr.rotateReverse(4);
		
		circleArr.print();
		
		Iterator<Boolean> it = circleArr.iterator();
		
		while(it.hasNext()) {
			  System.out.print(it.next() + " ");
			}
		
		System.out.println("\n" + circleArr.getArrCirIndex(2));
	}
	
	public static void ObjectCircle() {

		Object[] objectArr = new Object[] {1, "2", 3.33, 4L, false};
		
		CircularArray circleArr = new CircularArray(objectArr);
		
		System.out.println(Arrays.toString(circleArr.getCirArr()));
		
		circleArr.rotate(4);
		
		circleArr.print();
		
		circleArr.rotateReverse(4);
		
		circleArr.print();
		
		Iterator<Object> it = circleArr.iterator();
		
		while(it.hasNext()) {
			  System.out.print(it.next() + " ");
			}
		
		System.out.println("\n" + circleArr.getArrCirIndex(2));
	}
	
	public static void CharCircle() {

		Character[] charArr = new Character[] {'a', 'b', 'c', 'd', 'f'};
		
		CircularArray circleArr = new CircularArray(charArr);
		
		System.out.println(Arrays.toString(circleArr.getCirArr()));
		
		circleArr.rotate(4);
		
		circleArr.print();
		
		circleArr.rotateReverse(4);
		
		circleArr.print();
		
		Iterator<Character> it = circleArr.iterator();
		
		while(it.hasNext()) {
			  System.out.print(it.next() + " ");
			}
		
		System.out.println("\n" + circleArr.getArrCirIndex(2));
	}
	
	public static void ByteCircle() {

		Byte[] byteArr = new Byte[] {1, 2, 3, 4, 5};
		
		CircularArray circleArr = new CircularArray(byteArr);
		
		System.out.println(Arrays.toString(circleArr.getCirArr()));
		
		circleArr.rotate(4);
		
		circleArr.print();
		
		circleArr.rotateReverse(4);
		
		circleArr.print();
		
		Iterator<Byte> it = circleArr.iterator();
		
		while(it.hasNext()) {
			  System.out.print(it.next() + " ");
			}
		
		System.out.println("\n" + circleArr.getArrCirIndex(2));
	}
	
	public static void ShortCircle() {

		Short[] shortArr = new Short[] {1, 2, 3, 4, 5};
		
		CircularArray circleArr = new CircularArray(shortArr);
		
		System.out.println(Arrays.toString(circleArr.getCirArr()));
		
		circleArr.rotate(4);
		
		circleArr.print();
		
		circleArr.rotateReverse(4);
		
		circleArr.print();
		
		Iterator<Short> it = circleArr.iterator();
		
		while(it.hasNext()) {
			  System.out.print(it.next() + " ");
			}
		
		System.out.println("\n" + circleArr.getArrCirIndex(2));
	}
	
	public static void FloatCircle() {

		Float[] floatArr = new Float[] {1.1f, 2.2f, 3.3f, 4.4f, 5.5f};
		
		CircularArray circleArr = new CircularArray(floatArr);
		
		System.out.println(Arrays.toString(circleArr.getCirArr()));
		
		circleArr.rotate(4);
		
		circleArr.print();
		
		circleArr.rotateReverse(4);
		
		circleArr.print();
		
		Iterator<Float> it = circleArr.iterator();
		
		while(it.hasNext()) {
			  System.out.print(it.next() + " ");
			}
		
		System.out.println("\n" + circleArr.getArrCirIndex(2));
	}
}
